﻿#Prints to stdout. Piping output to a file is strongly recommended.
[CmdletBinding()]
param(
    [Parameter(Position = 0, Mandatory)]
    [Alias('Fullname', 'PSPath')]
    [ValidateScript({
        Test-Path $_
    })]
    [string]
    $FilePath,

    [Parameter(Position = 1)]
    [int]
    $LineLength = 100
)

$Bytes = [System.IO.File]::ReadAllBytes((Resolve-Path $FilePath))
$Base64Text = [System.Convert]::ToBase64String($Bytes)

#Begin output to be embedded into another script.
'$B64=New-Object Text.StringBuilder'
while($Base64Text.Length -gt $LineLength)
{
    '[void]$B64.Append("' + $Base64Text.Substring(0,$LineLength) + '")'
    $Base64Text = $Base64Text.Substring($LineLength)
}
'[void]$B64.Append("' + $Base64Text + '")'
'$Base64 = $B64.ToString()'